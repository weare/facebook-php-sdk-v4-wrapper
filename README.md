WE_ARE wrapper for Facebook SDK 4 for PHP
===============================================================================
Help build for the different Facebook platforms (App on Facebook, Website, Page Tab, ...)

Composer
-------------------------------------------------------------------------------
~~~json
{
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:weare/facebook-php-sdk-v4-wrapper.git"
        }
    ],
    "require": {
        "weare/facebook-php-sdk-v4-wrapper": "1.1.*-dev"
    }
}
~~~