<?php

namespace FacebookWrapper;

class FacebookRedirectLoginHelper extends \Facebook\FacebookRedirectLoginHelper
{
    private $sessionHandler;

    /**
    * @var string Prefix to use for session variables
    */
    private $sessionPrefix = 'FBRLH_';

    public function __construct($sessionHandler, $redirectUrl, $appId = null, $appSecret = null)
    {
        $this->sessionHandler = $sessionHandler;

        parent::__construct($redirectUrl, $appId, $appSecret);
    }

    /**
     * Stores a state string in session storage for CSRF protection.
     * Developers should subclass and override this method if they want to store
     *   this state in a different location.
     *
     * @param string $state
     *
     * @throws FacebookSDKException
     */
    protected function storeState($state)
    {
        $this->sessionHandler->set($this->sessionPrefix . 'state', $state);
    }

    /**
     * Loads a state string from session storage for CSRF validation.  May return
     *   null if no object exists.  Developers should subclass and override this
     *   method if they want to load the state from a different location.
     *
     * @return string|null
     *
     * @throws FacebookSDKException
     */
    protected function loadState()
    {
        return $this->sessionHandler->get($this->sessionPrefix . 'state');
    }
}