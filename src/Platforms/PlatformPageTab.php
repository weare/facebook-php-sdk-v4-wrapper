<?php
/**
 * Platform - Page Tab.
 *
 * @author WE_ARE
 */

namespace FacebookWrapper\Platforms;

use FacebookWrapper\FacebookRedirectLoginHelper;
use Facebook\FacebookPageTabHelper;
use Facebook\FacebookRequest;

class PlatformPageTab extends Platform implements PlatformInterface
{
    private $pageTabHelper;

    public function __construct($configs = array(), \FacebookWrapper\SessionHandler $sessionHandler = null)
    {
        parent::__construct($configs, $sessionHandler);

        // If no redirect URL is provided, there will only be a Javascript session init test.
        if (!empty($configs['platforms']['pageTab']['redirectUrl'])) {
            $this->redirectLoginHelper = new FacebookRedirectLoginHelper($this->sessionHandler, $configs['platforms']['pageTab']['redirectUrl']);
        }

        $this->pageTabHelper = new FacebookPageTabHelper();

        $this->initSession();
    }

    /**
     * @todo Return a status for debugging.
     * @todo Add possibility to retrieve the JavaScript SDK session.
     */
    public function initSession()
    {
        // Try from session.
        if (is_null($this->initSessionFromPHPSession())) {
            // Try from redirect.
            if (
                !isset($this->redirectLoginHelper)
                || (isset($this->redirectLoginHelper) && !$this->initSessionFromRedirect())
            ) {
                // Try from Javascript.
                $this->initSessionFromJavascript();
                // Try from the PageTabHelper.
                if (is_null($this->session)) {
                    // The session will be getted only if the user accepted the app.
                    $this->session = $this->pageTabHelper->getSession();
                }
            }
        }
    }

    public function isCurrentUserConnectedToTheApp()
    {
        $signedRequest = $this->pageTabHelper->getSignedRequest();

        return (bool) $signedRequest->getUserId();
    }

    public function isCurrentUserLikeCurrentPage()
    {
        return $this->pageTabHelper->isLiked();
    }

    /**
     * Get the connected user locale.
     *
     * @param bool $formatISO639_1
     *   Return the ISO 639-1 value of the locale (http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) if "true".
     *   If set to false, return the raw value from FB (https://developers.facebook.com/docs/internationalization#locales).
     *
     * @todo Add FacebookRequestException.
     *
     * @return string
     *   The current user locale.
     */
    public function getUserLocale($formatISO639_1 = true)
    {
        if ($this->session) {
            $request = new FacebookRequest($this->session, 'GET', '/me', array('fields' => 'locale'));
            $locale = $request->execute()->getGraphObject()->getProperty('locale');
            if ($locale && $formatISO639_1) {
                $locale = substr($locale, 0, 2);
            }
            return $locale;
        } else {
            $signedRequest = $this->pageTabHelper->getSignedRequest();
            if (isset($signedRequest->payload['user']['locale'])) {
                $locale = $signedRequest->payload['user']['locale'];
                if ($formatISO639_1) {
                    $locale = substr($locale, 0, 2);
                }
                return $locale;
            }
        }
    }
/**

**/



    /**
     *  Create the helper that will help us to most of SDK calls.
     */
    protected function createHelper()
    {
        $this->pageTabHelper = new FacebookPageTabHelper();
    }

    /**
     *  Get the user ID.
     *  TODO: maybe we should overwrite the inherited getMyId method here ?
     */
    public function getUserId()
    {
        return $this->pageTabHelper->getUserId();
    }

    /**
     *  Get the page ID.
     */
    public function getPageId()
    {
        return $this->pageTabHelper->getPageId();
    }

    /**
     *  Check if user liked the page or not.
     */
    public function isLiked()
    {
        return $this->pageTabHelper->isLiked();
    }

    /**
     *  Check if user is admin of the page.
     */
    public function isAdmin()
    {
        return $this->pageTabHelper->isAdmin();
    }
}
