<?php
/**
 * Platform - Web.
 *
 * @author WE_ARE
 */

namespace FacebookWrapper\Platforms;

use FacebookWrapper\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use \Exception;

class PlatformWeb extends Platform implements PlatformInterface
{
    public function __construct($configs = array(), \FacebookWrapper\SessionHandler $sessionHandler = null)
    {
        parent::__construct($configs, $sessionHandler);

        if (empty($configs['platforms']['web']['redirectUrl'])) {
            throw new \InvalidArgumentException('Config required: "platforms/web/redirectUrl"');
        }
        $this->redirectLoginHelper = new FacebookRedirectLoginHelper($this->sessionHandler, $configs['platforms']['web']['redirectUrl']);

        $this->initSession();
    }

    /**
     * @todo Return a status for debugging.
     * @todo Add possibility to retrieve the JavaScript SDK session.
     */
    public function initSession()
    {
        // Try from session.
        if (is_null($this->initSessionFromPHPSession())) {
            // Try from redirect.
            if (!$this->initSessionFromRedirect()) {
                // Try from Javascript.
                $this->initSessionFromJavascript();
            }
        }
    }

    public function isCurrentUserConnectedToTheApp()
    {
        return (bool) $this->session;
    }

    public function isCurrentUserAcceptThisScope($scope)
    {
        $scopes = $this->accessToken->getInfo()->getScopes();
        return array_search($scope, $scopes) !== false;
    }

    /**
     * NEED_USER_TO_BE_CONNECTED
     * NEED_USER_LIKES_SCOPE
     * false
     * true
     * 
     * @param string $pageID
     * @return boolean | string
     */
    public function isCurrentUserLikePage($pageID)
    {
        // Is current user connected to the app.
        if (!$this->isCurrentUserConnectedToTheApp()) {
            return self::NEED_USER_TO_BE_CONNECTED;
        }

        // Is current user accepted the scope "user_likes".
        if (!$this->isCurrentUserAcceptThisScope('user_likes')) {
            return self::NEED_USER_LIKES_SCOPE;
        }

        // Is current user like this page.
        $request = new FacebookRequest(
            $this->session,
            'GET',
            sprintf('/me/likes/%s', $pageID)
        );
        $response = $request->execute();
        $pageLikes = $response->getGraphObject()->asArray();

        if (!empty($pageLikes)) {
            return true;
        }

        return false;
    }
}
