<?php

/**
 * Platforms:
 * - App on Facebook (canvas);
 * - Website (web);
 * - Page Tab (pageTab);
 * - iOS (ios);
 * - Android (android);
 * - Windows App (windows);
 * - Xbox (xbox);
 * - PlayStation (playstation).
 */

namespace FacebookWrapper\Platforms;

interface PlatformInterface{
    public function isCurrentUserConnectedToTheApp();
    public function initSession();
}