<?php

/**
 * Platforms:
 * - App on Facebook (canvas);
 * - Website (web);
 * - Page Tab (pageTab);
 * - iOS (ios);
 * - Android (android);
 * - Windows App (windows);
 * - Xbox (xbox);
 * - PlayStation (playstation).
 */

namespace FacebookWrapper\Platforms;

use \FacebookWrapper\SessionHandler;
use \FacebookWrapper\AccessTokenHandler;
use Facebook\FacebookSession;
use Facebook\FacebookCanvasLoginHelper;
use Facebook\FacebookJavaScriptLoginHelper;
use Facebook\FacebookRequest;

class Platform
{
    // Status messages.
    const NEED_USER_TO_BE_CONNECTED = 'Need user to be connected';
    const NEED_USER_LIKES_SCOPE = 'Need "user_likes" scope';

    /**
     * Array of configuration for the app:
     * - appID: As provided by Facebook;
     * - appSecret: As provided by Facebook;
     * - scopes: Array of scopes permissions (https://developers.facebook.com/docs/facebook-login/permissions/v2.0)
     *           Additionnal scopes can be asked when asking for login url when
     *           you try to connect from the "Platform Web" for example. There's
     *           no need to ask for it if it's not absolutly needed;
     * - platforms: Array of platforms;
     *   - {platform}: Array of settings for this specific platform.
     * - debug: bool, show debug messages.
     */
    protected $configs = array();

    /**
     * Instance of "FacebookWrapper\FacebookRedirectLoginHelper".
     */
    protected $redirectLoginHelper;

    /**
     * The Facebook session used to get the access token.
     */
    protected $session;

    /**
     * The session handler.
     */
    protected $sessionHandler;

    /**
     * The access token used to make API calls.
     */
    protected $accessToken;

    /**
     * The access token handler.
     */
    protected $accessTokenHandler;

    /**
     * Constructor.
     * 
     * @param array $configs
     *   Array of configuration for the app. See the definition of self::$configs.
     * @param object $sessionHandler
     *   The session handler.
     */
    public function __construct($configs = array(), SessionHandler $sessionHandler = null)
    {
        // Default configs.
        $defaultConfigs = array(
            'appID' => '',
            'appSecret' => '',
            'scopes' => array(),//@todo: check if sub-array are merged too by "array_merge".
            'platforms' => array(
                'web' => array(
                    'redirectUrl' => '',
                ),
                'pageTab' => array(
                    'redirectUrl' => '',
                ),
            ),
            'debug' => false,
        );
        // Merge configs.
        $this->configs = array_merge($defaultConfigs, $configs);

        // If no SessionHandler is passed, use the default one.
        if (is_null($sessionHandler)) {
            $this->sessionHandler = new SessionHandler();
        } else {
            $this->sessionHandler = $sessionHandler;
        }

        $this->accessTokenHandler = new AccessTokenHandler($this->sessionHandler);

        // Initialize the SDK.
        FacebookSession::setDefaultApplication($this->configs['appID'], $this->configs['appSecret']);
    }

    /**
     * Get the access token object from the session.
     *
     * If it's not valid, invalid the session and return null.
     *
     * @return Facebook\Entities\AccessToken
     *   The access token object.
     */
    protected function getAccessToken()
    {
        $this->accessToken = $this->session->getAccessToken();
        if (!$this->accessToken->isValid()) {
            $this->logout();
            return null;
        }

        return $this->accessToken;
    }

    /**
     * Get a session from an access token.
     *
     * The access token could have been passed through a AJAX call for example.
     *
     * @return Facebook\Entities\AccessToken
     *   The access token object.
     */
    public function initSessionFromAccessToken($accessToken)
    {
        $this->session = new FacebookSession($accessToken);
        return $this->getAccessToken();
    }

    /**
     * Get a session from PHP $_SESSION.
     *
     * @return Facebook\Entities\AccessToken
     *   The access token object.
     */
    protected function initSessionFromPHPSession()
    {
        if ($this->accessTokenHandler->has()) {
            $this->session = new FacebookSession($this->accessTokenHandler->get());
            return $this->getAccessToken();
        }

        return null;
    }

    /**
     * Get a session from a redirect.
     *
     * The client go to a Facebook connexion page (use "getLoginUrl()" to get it)
     * and Facebook redirect him with params that could be used to get a session.
     * 
     * @todo Return a status for debugging.
     * @see https://developers.facebook.com/docs/php/gettingstarted/4.0.0
     *
     * @return Facebook\Entities\AccessToken
     *   The access token object.
     */
    protected function initSessionFromRedirect()
    {
        // Maybe a login connection has been initialized.
        try {
            $this->session = $this->redirectLoginHelper->getSessionFromRedirect();
            if ($this->session && !is_null($this->getAccessToken())) {
                $this->accessTokenHandler->set($this->accessToken);
                return $this->accessToken;
            }
        }
        catch (FacebookRequestException $ex) {
            $this->debug($ex->getMessage(), "red");
        }
        catch (Exception $ex) {
            $this->debug($ex->getMessage(), "red");
        }

        return null;
    }

    protected function initSessionFromCanvas()
    {
        $helper = new FacebookCanvasLoginHelper();
        try {
            $this->session = $helper->getSession();
        } catch(FacebookRequestException $ex) {
            // When Facebook returns an error
        } catch(\Exception $ex) {
            // When validation fails or other local issues
        }
    }

    protected function initSessionFromJavascript()
    {
        $helper = new FacebookJavaScriptLoginHelper();
        try {
            $this->session = $helper->getSession();
            if ($this->session && !is_null($this->getAccessToken())) {
                $this->accessTokenHandler->set($this->accessToken);
                return $this->accessToken;
            }
        } catch(FacebookRequestException $ex) {
            // When Facebook returns an error
        } catch(\Exception $ex) {
            // When validation fails or other local issues
        }

        return null;
    }

    // public function isCurrentUserConnected()
    // public function isCurrentUserLikePage()

    public function isCurrentUserAcceptedScopes()
    {

    }

    /**
     * Return the login URL.
     *
     * @param array|string $additionnalScopes
     */
    public function getLoginUrl($additionnalScopes = array())
    {
        return $this->redirectLoginHelper->getLoginUrl(array_merge($this->configs['scopes'], (array) $additionnalScopes));
    }

    /**
     * Make a call to the Facebook Graph API.
     *
     * @link https://developers.facebook.com/docs/graph-api/reference/v2.1/
     *
     * @param string $path
     *   The path.
     * @param array $parameters
     *   Params accept by the Graph API.
     * @param string $method
     *   The method.
     *
     * @return array
     *   The Graph Object as array.
     */
    public function request($path = '/me', $parameters = null, $method = 'GET')
    {
        if (!is_null($this->session)) {
            $request = new FacebookRequest($this->session, $method, $path);
            $response = $request->execute();
            return $response->getGraphObject()->asArray();
        }

        return null;
    }

    /**
     * Remove the AccessToken and forget the Facebook Session.
     */
    public function logout()
    {
        $this->accessTokenHandler->remove();
        $this->session = null;
    }

    /**
     *  A debug function that prints stuff.
     */
    public function debug($a, $color = "gray", $title = "")
    {
        if($this->configs->debug === false)
            return false;

        if($title)
            echo "<h3 style=\"margin:50px 0px 0px 10px; text-align:left; color:$color;\">$title :</h3>";

        echo '<pre style="text-align:left; border:1px solid '.$color.'; padding:10px; margin:10px; color:'.$color.'; overflow: auto; max-height: 500px;">'; print_r($a); echo '</pre>';
    }
}