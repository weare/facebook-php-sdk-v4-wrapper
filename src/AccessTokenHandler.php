<?php

namespace FacebookWrapper;

use \Facebook\Entities\AccessToken;

class AccessTokenHandler
{
    private $sessionHandler;

    public function __construct($sessionHandler)
    {
        $this->sessionHandler = $sessionHandler;
    }

    public function has()
    {
        return $this->sessionHandler->has('accessToken');
    }

    public function get()
    {
        return $this->sessionHandler->get('accessToken');
    }

    public function set(AccessToken $accessToken)
    {
        $this->sessionHandler->set('accessToken', $accessToken);

        return true;
    }

    public function remove()
    {
        return $this->sessionHandler->remove('accessToken');
    }
}
