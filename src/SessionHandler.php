<?php

/**
 * Class to be extended by your app for the wrapper to persist session infos like access token.
 */

namespace FacebookWrapper;

use \App\Config;
use \Facebook\FacebookSDKException;

class SessionHandler
{
    public function has($key)
    {
        return isset($_SESSION[$key]);
    }

    public function get($key)
    {
        if (session_status() !== PHP_SESSION_ACTIVE) {
            throw new FacebookSDKException(
                'Session not active, could not load state.', 721
            );
        }
        if ($this->has($key)) {
            return $_SESSION[$key];
        } else {
            return null;
        }
    }

    public function set($key, $value)
    {
        if (session_status() !== PHP_SESSION_ACTIVE) {
            throw new FacebookSDKException(
                'Session not active, could not store state.', 720
            );
        }
        $_SESSION[$key] = $value;

        return true;
    }

    public function remove($key)
    {
        unset($_SESSION[$key]);

        return true;
    }
}
